# Markdown
A Markdown library,
that may parse according
to Common Markdown or
to whatever you think of.

Or anything in between.

# Common Markdown
To parse a common markdown
string and convert
it to `Html msg`:

```elm
import Markdown exposing (toHtml, common)
import Html exposing (Html)

source = """
# Hello, World
"""

html : Html msg
html = toHtml common source

-- html == h1 [ text "Hello, World" ]
```

# Extensibility
To extend or modify
the common implementation
you can do so easily.

Because common is
just a List of Markdown Elements:
```elm
common : List Markdown.Element
common =
  [ rule headingParser headingRenderer
  , rule codeParser codeRenderer
  ...
  ]
```
Each element of the List
is a rule, each rule has a parser
and a renderer part.
Whatever you
